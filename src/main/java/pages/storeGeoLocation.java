package pages;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.driverInitiate;
import utils.caputerScreenshot;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class storeGeoLocation extends driverInitiate {

    private static By alert = By.xpath("//*[@id=\'map\']/div[2]/table/tr/td[2]/button");

    public static void click_Alert_Popup()
    {
        Alert alert = driver.switchTo().alert();
        alert.accept();
    }

    public static void mock_Location()
    {
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://automationpractice.com/index.php?controller=stores");

        /*Alert alert = driver.switchTo().alert();
        alert.accept();
        */
        waitforelement();
        driver.findElement(alert).click();

        Map coordinates = new HashMap()
        {{
            put("latitude",26.709723);
            put("longitude",-80.064163);
            put("accuracy", 1);
        }};
        driver.executeCdpCommand("Emulation.setGeolocationOverride", coordinates);
        //driver.get("http://automationpractice.com/index.php?controller=stores");
    }

    public static void waitforelement()
    {
        WebDriverWait wait = new WebDriverWait(driver, 50);//Wait Function Use
        WebElement wait2 = wait.until(ExpectedConditions.elementToBeClickable(alert));
    }

    public static void capture_sc() throws IOException
    {
        caputerScreenshot.Screenshot(driver,"StoreLocatorPage"+"-");
        //captureScreenshot.Screenshot(driver,"Login-"+uname+"-");
    }
}
