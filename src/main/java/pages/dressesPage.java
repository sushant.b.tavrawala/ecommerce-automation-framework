package pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import utils.driverInitiate;
import java.util.*;

public class dressesPage extends driverInitiate {

    static float maxPrice = 0;
    static float highPrice = (float) 50.99;

    public static void validate_Price() throws InterruptedException {
        List<WebElement> prices = driver.findElements(By.cssSelector(".right-block [itemprop = 'price']"));
        for(int i = 0; i < prices.size(); i++)
        {
            String newStr = prices.get(i).getText().replaceAll("[^\\d.]","");
            float price = Float.parseFloat(newStr);

            if(price > maxPrice) {
                maxPrice = price;
            }
            System.out.println(price);
            if(maxPrice == highPrice)
            {
                System.out.println("This is MaxPrice: " + maxPrice);
                WebElement clickItem = driver.findElement(By.cssSelector(".right-block h5 a.product-name"));
                clickItem.click();
                driver.findElement(By.id("add_to_cart")).click();
                WebElement message = driver.findElement(By.xpath("//*[@id='layer_cart']/div[1]/div[1]/h2/text()"));
                Assert.assertEquals(message.getText(), "Product successfully added to your shopping cart ");
            }
        }

    }

    public static void find_Max_Price_Item() throws InterruptedException {

        List<WebElement> pricesList = driver.findElements(By.cssSelector(".right-block [itemprop = 'price']"));
        ArrayList<Float> priceListSorted = new ArrayList<>();

        pricesList.forEach(p -> priceListSorted.add(Float.parseFloat(p.getText().replaceAll("\\$", "").trim())));
        Collections.sort(priceListSorted);

        System.out.println("------------" + priceListSorted);
        String maxP_xpath = "//div[@class='right-block']//span[contains(text(),'$" + priceListSorted.get(priceListSorted.size()-1) + "')]/../../h5/a";
        System.out.println("-----------"+ maxP_xpath);
        driver.findElement(By.xpath(maxP_xpath)).click();

        driver.findElement(By.xpath("//p[@id='add_to_cart']/button")).click();
        Thread.sleep(4000);
    }

}
