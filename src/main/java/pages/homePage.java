package pages;

import org.openqa.selenium.By;
import utils.driverInitiate;

public class homePage extends driverInitiate {

    private static final By Faded_Short_Sleeve_Tshirts = By.linkText("Faded Short Sleeve T-shirts");
    private static final By Menu_Item_Dresses = By.linkText("DRESSES");

    public static void click_Dresses_Menu_Link() throws InterruptedException {
            driver.findElement(Menu_Item_Dresses).click();
    }

    public static void select_Item_To_addCart(String item) {

        if (item.equalsIgnoreCase("Faded Short Sleeve T-shirts")) {
            driver.findElement(Faded_Short_Sleeve_Tshirts).click();

        }
    }
}
