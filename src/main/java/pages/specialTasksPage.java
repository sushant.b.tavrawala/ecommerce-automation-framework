package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import utils.driverInitiate;

import java.util.LinkedList;
import java.util.List;

public class specialTasksPage extends driverInitiate {

    Actions actions;

    private static By navigate_Women_Menu = By.xpath("//*[@id='block_top_menu']/ul/li[1]/a");
    private static By navigate_Women_Menu_Tags = By.xpath("//*[@id='block_top_menu']/ul/li[1]");
    private static By navigate_Right_Slidebar = By.xpath("//*[@id=\'layered_price_slider\']/a[2]");
    private static String expected_price = "$16.00 - $20.00";


    //HoverOperation
    public static void Hover_Women_Summer_Dresses()
    {
        WebElement Menu_Women = driver.findElement(navigate_Women_Menu);
        Actions action = new Actions(driver);
        action.moveToElement(Menu_Women).build().perform();
    }

    public static void select_Navigation_Link(String link)
    {
        WebElement Women_Menu_Tags = driver.findElement(navigate_Women_Menu_Tags );
        List<WebElement> tags = Women_Menu_Tags.findElements(By.tagName("a"));
       // System.out.println("number of elements: "+ tags.size());

        for (WebElement menu: tags)
        {
            if (menu.getText().equalsIgnoreCase(link))
            {
                menu.click();
                break;
            }
        }
    }
    //Price range slider
    public static void move_Slidebar(boolean isLeft, int number) throws InterruptedException {
        Actions action = new Actions(driver);
        WebElement rightslidebar = driver.findElement(navigate_Right_Slidebar);
        rightslidebar.click();

        WebElement priceElement = driver.findElement(By.xpath("//*[@id='layered_price_range']"));

        if(isLeft)
        {
            for(int i =0;i<number;i++)
            {
            action.sendKeys(Keys.ARROW_RIGHT).build().perform();
            }
        }else
        {
            {
                for(int i =0;i<number;i++)
                {
                    action.sendKeys(Keys.ARROW_LEFT).build().perform();
                }
            }
        }
    }

    public static void validate_Price()
    {
        List<WebElement> prices = driver.findElements(By.cssSelector("span.price.product-price"));
        List<Float> rangePrices = new LinkedList<Float>();
        for(WebElement elm: prices)
        {
            if (elm.getText().length()!=0)
            {
                String newStr = elm.getText().replaceAll("[^\\d.]","");
                float price = Float.parseFloat(newStr);
                if(price > 20)
                {
                    rangePrices.add(price);
                    System.out.println("Filter is not working as price is greater than range :" + price);
                    break;
                }
            }
        }
    }
}
