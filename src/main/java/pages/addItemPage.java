package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.driverInitiate;

public class addItemPage extends driverInitiate {

    private static By Add_To_Cart_Button = By.xpath("//*[@id='add_to_cart']/button");
    private static By Proceed_To_Checkout =By.xpath("//*[@id='layer_cart']/div[1]/div[2]/div[4]/a");

    public static void add_Item_To_Cart() {
        waitforelement();
        driver.findElement(Add_To_Cart_Button).click();
    }

    public static void refresh_Page()
    {
        driver.navigate().refresh();
    }

    public static void waitforelement()
    {
        WebDriverWait wait = new WebDriverWait(driver, 50);//Wait Function Use
        WebElement wait2 = wait.until(ExpectedConditions.elementToBeClickable(Add_To_Cart_Button));
    }
}