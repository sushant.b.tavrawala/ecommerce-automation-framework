package pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import utils.driverInitiate;

public class proceedToCheckoutPage extends driverInitiate {


    private static By Proceed_To_Checkout = By.xpath("//*[@id=\'header\']/div[3]/div/div/div[3]/div/a");
    private static By Delete_Button = By.xpath("//*[@id=\'1_1_0_0\']");

    public static void click_Proceed_To_Checkout()
    {
        driver.findElement(Proceed_To_Checkout).click();
    }

    public static void Check_If_Delete_Button_Displayed()
    {
        Assert.assertTrue(find(Delete_Button).isDisplayed());
    }

    public static void Click_Delete_Button()
    {
        driver.findElement(Delete_Button).click();
    }
    public static void Check_If_EmptyCart_Message_Displayed(String message)
    {
        Assert.assertTrue(driver.getPageSource().contains(message));
    }
}
