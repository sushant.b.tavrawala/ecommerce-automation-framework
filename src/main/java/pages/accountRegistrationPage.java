package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.driverInitiate;

public class accountRegistrationPage extends driverInitiate {

    private static By EmailId = By.id("email_create");
    private static By CreateAccount_Button = By.id("SubmitCreate");

    //Title
    private static By click_Mr = By.id("id_gender1");
    private static By click_Mrs = By.id("id_gender2");

    //FirstName,Lastname,Password
    private static By FirstName = By.id("customer_firstname");
    private static By LastName = By.id("customer_lastname");
    private static By Password = By.id("passwd");

    //DOB
    private static By Day = By.id("days");
    private static By Month = By.id("months");
    private static By Year = By.id("years");

    //Address
    private static By Address = By.id("address1");
    private static By City = By.id("city");
    private static By State = By.id("id_state");
    private static By Postcode = By.id("postcode");

    //ContactNumber
    private static By MobilePhone = By.id("phone_mobile");

    //Register
    private static By Register_Button = By.id("submitAccount");

    //Validation
    private static String Duplicate_EmailAddress_ErrorMessage = "An account using this email address has already been registered. Please enter a valid password or request a new one.";
    private static String Expected_My_Account_PageTitle = "My account";
    private static By First_Last_Name_On_Account_Page = By.xpath("//*[@id=\'header\']/div[2]/div/div/nav/div[1]/a/span");

    public static void enter_EmailAddress(String email) {
        driver.findElement(EmailId).sendKeys(email);
        if(driver.getPageSource().contains(Duplicate_EmailAddress_ErrorMessage)) {
            System.out.println("Test Failed: Found duplicate email address");
        } else {
            System.out.println("Done!!!");
        }
    }

    public static void click_CreateAccount_Button() {
        driver.findElement(CreateAccount_Button).click();
    }

    public static void click_Title(String title) throws InterruptedException {
        waitforelement();
        if (title.equalsIgnoreCase("Mr."))
        {
            driver.findElement(click_Mr).click();
        }else
        {
            driver.findElement(click_Mrs).click();
        }
    }

    public static void enter_FirstName_LastName(String firstname, String lastname)
    {
        driver.findElement(FirstName).sendKeys(firstname);
        driver.findElement(LastName).sendKeys(lastname);
    }

    public static void enter_Password(String password)
    {
        driver.findElement(Password).sendKeys(password);
    }

    public static void select_Dob(String day, String month, String year)
    {
        WebElement dob_Day = driver.findElement(Day);
        Select sel_Day = new Select(dob_Day);
        sel_Day.selectByValue(day);

        WebElement dob_Month = driver.findElement(Month);
        Select sel_Month = new Select(dob_Month);
        sel_Month.selectByValue(month);

        WebElement dob_Year = driver.findElement(Year);
        Select sel_Year = new Select(dob_Year);
        sel_Year.selectByValue(year);
    }

    public static void enter_Address(String address)
    {
        driver.findElement(Address).sendKeys(address);
    }

    public static void enter_City(String city)
    {
        driver.findElement(City).sendKeys(city);
    }

    public static void select_State(String state)
    {
        WebElement sel_state = driver.findElement(State);
        Select sel_State = new Select(sel_state);
        sel_State.selectByVisibleText(state);
    }

    public static void enter_Postcode(String postcode)
    {
        driver.findElement(Postcode).sendKeys(postcode);
    }

    public static void enter_MobilePhone(String mobileNumber)
    {
        driver.findElement(MobilePhone).sendKeys(mobileNumber);
    }

    public static void click_Register_Button()
    {
        driver.findElement(Register_Button).click();
    }

    public static void my_Account_Page_Verification()
    {
        if(driver.getPageSource().contains(Expected_My_Account_PageTitle)){
            System.out.println(Expected_My_Account_PageTitle);
        }else{
            System.err.println("FAILED");
        }
    }

    public static void name_Validation(String expectedName)
    {
        if(driver.getPageSource().contains(expectedName)){
            System.out.println(expectedName);
        }else{
            System.err.println("Name validation FAILED");
        }
    }

    public static void FirstName_ErrorMessage(String message)
    {
        if (driver.getPageSource().contains(message))
        {
            System.out.println("FirstName Error: "+message);
        }
    }
    public static void waitforelement()
    {
        WebDriverWait wait = new WebDriverWait(driver, 50);//Wait Function Use
        WebElement wait2 = wait.until(ExpectedConditions.elementToBeClickable(FirstName));
    }
}
