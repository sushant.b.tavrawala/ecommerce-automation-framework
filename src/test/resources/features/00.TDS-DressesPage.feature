Feature: Navigate to the website and Add highest price item to the cart

  Background:
    Given the browser is initiated with url "http://automationpractice.com/index.php"

  Scenario: 1- Add highest price item to the cart
    When Click on the Dresses Menu
    Then Add the selected highest price item to the cart