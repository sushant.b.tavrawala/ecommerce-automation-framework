Feature: Navigate to the website and perform actions related to story 1,2,3

  Background:
    Given the browser is initiated with url "http://automationpractice.com/index.php"

  Scenario: 1- user is able to delete the item from cart
    When I added the "Faded Short Sleeve T-shirts" to the cart
    Then added item is available in the cart with delete button
    And I removed the item from the cart
    And the message displayed "Your shopping cart is empty."

  Scenario: 2- User navigate to Summer Dresses page from the navigation menu
    When I hover mouse to Woman navigation option appear
    Then I navigate to the page "Summer Dresses"

  Scenario: 3- User navigate to the page and change the price range to see the results
    When I hover mouse to Woman navigation option appear
    And I navigate to the page "Summer Dresses"
    Then I move the price range slidebar move to 75
    And I validate the price range

