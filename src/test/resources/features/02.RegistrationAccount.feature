Feature: Navigate to the website and perform various actions

  Background:
    Given the browser is initiated with url "http://automationpractice.com/index.php?controller=authentication&back=my-account"

  Scenario Outline: 1- user succesfully create an account
    When User added "<Email>" and click create an account button
    And User select option "<Title>"
    And User enter firstname "<FirstName>" and lastname "<LastName>"
    And User enter password "<Password>"
    And User select DOB "<Day>","<Month>","<Year>"
    And User enter address "<Address>"
    And User enter city "<City>"
    And User select state "<State>"
    And User enter postcode "<ZipPostalCode>"
    And User enter mobile number "<MobilePhone>"
    Then User click on Register button
    And User navigate to the My account page
    And User "<expectedName>" is displayed on the top right corner

    Examples:
      | Email            | Title | FirstName | LastName | Password | Day | Month | Year | Address      | City  | State | ZipPostalCode | MobilePhone | expectedName   |
      | sam5@yopmail.com | Mr.   | Samfive   | Nelson   | abc1234  | 5   | 12    | 1999 | 12 Mark Road | Texas | Texas | 75201         | 07738709400 | Samfive Nelson |

  Scenario: 2- Invalid First Name fields will give an error message
    When User added "sam2569@yopmail.com" and click create an account button
    And User select option "Mr."
    And User enter firstname "Sam1" and lastname "Nelson"
    And User enter password "abc123"
    Then User click on Register button
    And User get the first name error "firstname is invalid."

  Scenario: 3- Empty compulsory blank fields will give an error message
    When User added "sam2569@yopmail.com" and click create an account button
    And User select option "Mr."
    And User enter firstname "Sam1" and lastname "Nelson"
    And User enter password ""
    Then User click on Register button
    And User get the first name error "passwd is required."