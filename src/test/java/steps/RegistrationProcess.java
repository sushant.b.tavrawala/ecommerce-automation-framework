package steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pages.accountRegistrationPage;

public class RegistrationProcess {
    @When("User added {string} and click create an account button")
    public void userAddedAndClickCreateAnAccountButton(String email) {
        accountRegistrationPage.enter_EmailAddress(email);
        accountRegistrationPage.click_CreateAccount_Button();
    }

    @And("User select option {string}")
    public void userSelectOption(String title) throws InterruptedException {
        accountRegistrationPage.click_Title(title);
    }


    @And("User enter firstname {string} and lastname {string}")
    public void userEnterFirstnameAndLastname(String firstname, String lastname) {
        accountRegistrationPage.enter_FirstName_LastName(firstname,lastname);
    }

    @And("User enter password {string}")
    public void userEnterPassword(String password) {
        accountRegistrationPage.enter_Password(password);
    }

    @And("User select DOB {string},{string},{string}")
    public void userSelectDOB(String day, String month, String year) {
        accountRegistrationPage.select_Dob(day,month,year);
    }

    @And("User enter address {string}")
    public void userEnterAddress(String address) {
        accountRegistrationPage.enter_Address(address);
    }

    @And("User enter city {string}")
    public void userEnterCity(String city) {
        accountRegistrationPage.enter_City(city);
    }

    @And("User select state {string}")
    public void userSelectState(String state) {
        accountRegistrationPage.select_State(state);
    }

    @And("User enter postcode {string}")
    public void userEnterPostcode(String postcode) {
        accountRegistrationPage.enter_Postcode(postcode);
    }

    @And("User enter mobile number {string}")
    public void userEnterMobileNumber(String mobilenumber) {
        accountRegistrationPage.enter_MobilePhone(mobilenumber);
    }

    @Then("User click on Register button")
    public void userClickOnRegisterButton() {
        accountRegistrationPage.click_Register_Button();
    }

    @And("User navigate to the My account page")
    public void userNavigateToTheMyAccountPage() {
        accountRegistrationPage.my_Account_Page_Verification();
    }

    @And("User {string} is displayed on the top right corner")
    public void userIsDisplayedOnTheTopRightCorner(String expectedname) {
        accountRegistrationPage.name_Validation(expectedname);
    }

    @And("User get the first name error {string}")
    public void userGetTheFirstNameError(String errorMessage) {
        accountRegistrationPage.FirstName_ErrorMessage(errorMessage);
    }
}
