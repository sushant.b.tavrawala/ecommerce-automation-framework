package steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pages.specialTasksPage;

public class HoverNavigation {
    @When("I hover mouse to Woman navigation option appear")
    public void iHoverMouseToWomanNavigationOptionAppear() {
        specialTasksPage.Hover_Women_Summer_Dresses();
    }

    @Then("I navigate to the page {string}")
    public void iClick(String link) {
    specialTasksPage.select_Navigation_Link(link);
    }

  @Then("I move the price range slidebar move to {int}")
    public void iMoveThePriceRangeSlidebarTo(int price) throws InterruptedException {
        specialTasksPage.move_Slidebar(false,price);
    }

    @And("I validate the price range")
    public void iValidateThePriceRange() {
        specialTasksPage.validate_Price();
    }
}
