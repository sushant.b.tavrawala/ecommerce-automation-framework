package steps;

import io.cucumber.java.PendingException;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pages.homePage;
import pages.dressesPage;

public class Dresses {

    @When("^Click on the Dresses Menu$")
    public void click_on_the_dresses_menu() throws Throwable {
        homePage.click_Dresses_Menu_Link();
    }

    @Then("^Add the selected highest price item to the cart$")
    public void add_the_selected_highest_price_item_to_the_cart() throws Throwable {
        dressesPage.find_Max_Price_Item();
    }


}
