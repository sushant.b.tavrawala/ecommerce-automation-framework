package steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pages.addItemPage;
import pages.homePage;
import pages.navigateToUrl;
import pages.proceedToCheckoutPage;

public class ValidateDeleteItem {

    @Given("the browser is initiated with url {string}")
    public void theBrowserIsInitiatedWithUrl(String url) {
        navigateToUrl.navigate_Url(url);
    }

    @When("I added the {string} to the cart")
    public void iAddedTheToTheCart(String item) {
        homePage.select_Item_To_addCart(item);
        addItemPage.add_Item_To_Cart();
        addItemPage.refresh_Page();
    }

    @Then("added item is available in the cart with delete button")
    public void addedItemIsAvailableInTheCartWithDeleteButton() {
        proceedToCheckoutPage.click_Proceed_To_Checkout();
        proceedToCheckoutPage.Check_If_Delete_Button_Displayed();
    }

    @And("I removed the item from the cart")
    public void iRemovedTheItemFromTheCart() {
        proceedToCheckoutPage.Click_Delete_Button();
    }

    @And("the message displayed {string}")
    public void theMessageDisplayed(String message) {
        proceedToCheckoutPage.Check_If_EmptyCart_Message_Displayed(message);
    }


}
