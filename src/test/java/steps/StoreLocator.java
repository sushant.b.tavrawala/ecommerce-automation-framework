package steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import pages.storeGeoLocation;

import java.io.IOException;

public class StoreLocator {
    @Given("user is on our store page")
    public void userIsOnOurStorePage() {
        //storeGeoLocation.click_Alert_Popup();
        storeGeoLocation.mock_Location();
    }

    @And("user take a screenshot of the page with west palm beach")
    public void userTakeAScreenshotOfThePageWithWestPalmBeach() throws IOException {
        storeGeoLocation.capture_sc();
    }
}
